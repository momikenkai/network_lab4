package client;

import java.io.IOException;

public class Main {
    public static void main(String args[]){
        try {
            new client.Client().start();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
