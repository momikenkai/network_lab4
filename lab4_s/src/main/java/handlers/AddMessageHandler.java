package handlers;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import json.Message;
import server.ServerData;

public class AddMessageHandler extends HttpHandler {
    @Override
    public void process(HttpServerExchange exchange, ServerData serverData) {
        exchange.getRequestReceiver().receiveFullBytes((ex, data) ->{
            //System.out.println();
            Message message = gson.fromJson(new String(data), Message.class);
            String s = exchange.getRequestHeaders().getFirst("Authorization");
            s = s.replaceAll("\\D+","");
            int token = Integer.valueOf(s);
            serverData.updateTime(token);
            serverData.addMessage(message, token);
            exchange.setStatusCode(200);
            exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
            exchange.getResponseSender().send("");
        });

    }
}
