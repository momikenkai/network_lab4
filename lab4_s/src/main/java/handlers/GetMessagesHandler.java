package handlers;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import json.Message;
import json.MessageList;
import server.ServerData;

import java.util.List;

public class GetMessagesHandler extends HttpHandler {
    @Override
    public void process(HttpServerExchange exchange, ServerData serverData){
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {
            String s = exchange.getRequestHeaders().getFirst("Authorization");
            s = s.replaceAll("\\D+","");
            int token = Integer.valueOf(s);
            serverData.updateTime(token);
            int offset = Integer.valueOf(exchange.getQueryParameters().get("offset").getFirst());
            int count = Integer.valueOf(exchange.getQueryParameters().get("count").getFirst());
            List<Message> list = serverData.getMessages(offset, count);
            MessageList messageList = new MessageList();
            messageList.setMessages(list);
            String g = gson.toJson(messageList);
            exchange.setStatusCode(200);
            exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
            exchange.getResponseSender().send(g);
        });
    }
}
