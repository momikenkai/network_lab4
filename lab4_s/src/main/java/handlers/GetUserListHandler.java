package handlers;

import io.undertow.server.HttpServerExchange;
import io.undertow.util.HttpString;
import json.UserInfo;
import json.UsersList;
import server.ServerData;

import java.util.List;

public class GetUserListHandler extends HttpHandler {
    @Override
    public void process(HttpServerExchange exchange, ServerData serverData){
        exchange.getRequestReceiver().receiveFullBytes((ex, data) -> {
            String s = exchange.getRequestHeaders().getFirst("Authorization");
            s = s.replaceAll("\\D+","");
            int token = Integer.valueOf(s);
            serverData.updateTime(token);
            List<UserInfo> list = serverData.getUsers();
            UsersList usersList = new UsersList(list);
            String g = gson.toJson(usersList);
            exchange.setStatusCode(200);
            exchange.getResponseHeaders().add(new HttpString("Content-Type"), "application/json");
            exchange.getResponseSender().send(g);
        });
    }
}
