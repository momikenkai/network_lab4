package handlers;

import com.google.gson.Gson;
import io.undertow.server.HttpServerExchange;
import server.ServerData;

public class HttpHandler {
    protected Gson gson = new Gson();
    public void process(HttpServerExchange exchange, ServerData serverData){
        exchange.getRequestReceiver().receiveFullBytes((ex, data) ->{
            exchange.setStatusCode(405);
            exchange.getResponseSender().send("");
        });
    }
}
