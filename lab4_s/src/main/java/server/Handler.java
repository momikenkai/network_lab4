package server;

import handlers.*;
import io.undertow.util.HttpString;
import io.undertow.util.Methods;


public class Handler {
    HttpHandler getHandler(String path, HttpString method){
        HttpHandler httpHandler = new HttpHandler();
        String[] split = path.split("/");
        if(split[1].equals("login") && method.equals(Methods.POST)){
            httpHandler = new LoginHandler();
        } else if(split[1].equals("logout") && method.equals(Methods.POST)){
            httpHandler = new LogoutHandler();
        } else if(split[1].equals("users") && method.equals(Methods.GET)){
            httpHandler = new GetUserListHandler();
        } else if(split[1].equals("messages") && method.equals(Methods.GET)){
            httpHandler = new GetMessagesHandler();
        } else if(split[1].equals("messages") && method.equals(Methods.POST)){
            httpHandler = new AddMessageHandler();
        }
        return httpHandler;
    }
}
