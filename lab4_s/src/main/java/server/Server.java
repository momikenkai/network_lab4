package server;

import io.undertow.Undertow;

import java.util.Timer;

public class Server {
    private Undertow server;
    Server(){
        ServerData serverData = new ServerData();
        Timer timer = new Timer(true);
        timer.schedule(serverData, 3000, 3000);
        server = Undertow.builder().addHttpListener(8081,"localhost").setHandler(exchange -> (
                new Handler()).getHandler(
                exchange.getRelativePath(),
                exchange.getRequestMethod()).process(exchange, serverData)).build();

    }

    public void start(){
        server.start();
    }
}
